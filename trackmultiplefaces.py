#!/usr/bin/env python3
'''
    Author: Benoît Piédallu < benoit@laquadrature.net>
    Based on Guido Diepen <gdiepen@deloitte.nl> work
'''

#Import the OpenCV and dlib libraries
import cv2
import dlib
from colour import Color

from threading import Thread
import time
import random
import json # To write and read windows positions

import randomnames

#Initialize a face cascade using the frontal face haar cascade provided with
#the OpenCV library
#Make sure that you copy this file from the opencv project to the root of this
#project folder
faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

#The deisred output width and height
OUTPUT_SIZE_WIDTH = 775
OUTPUT_SIZE_HEIGHT = 600

# Colors
WHITE  =(255, 255, 255)
RED    = (0, 0, 255)
BLUE   = (255, 0, 0)
PURPLE = (127, 0, 255)
ORANGE = (0,165,255)
GREEN  = (0, 255, 0)
GREY   = (128,128,128)

suspiciousColors = {0:(0,0,0), 1:GREEN, 2:ORANGE, 3:RED}

avSusRectWidth = 20

cursorSize = (25, 25)
cursor = cv2.imread('logo.png')   # Read the file
cursor = cv2.resize(cursor, cursorSize)

averageSuspicious = 0

NAME_COLOR        = WHITE
IDENTIFYING_COLOR = WHITE


# TEXT
FONT_SIZE = 0.5

# The default color of the rectangle we draw around the face
rectangleColor = (0,165,255)
rectangleColorSuspicious = RED

# Suspicious threshold
SUSPICIOUS_THRESHOLD = 2

# Get the number of cameras available
def list_available_cameras():
    loglevel = cv2.getLogLevel()
    cv2.setLogLevel(2)

    cameras = []
    for i in range(10):
        temp_camera = cv2.VideoCapture(i)
        if temp_camera.isOpened():
            temp_camera.release()
            cameras.append(i)

    cv2.setLogLevel(loglevel)
    print("Found cameras:", cameras)
    return cameras

class facetracker:

#We are not really doing face recognition
    # def doRecognizePerson(self, faceNames, fid):
    #     time.sleep(2)
    #     faceNames[ fid ] = randomnames.rand_name_statistic(charset) #"Person " + str(fid)

    def displayGradientOn(self, resultImage, value, vertical=False):
        red = Color("red")
        orange = Color("orange")

        colors = list(red.range_to(orange,10))
        colors += list(orange.range_to(Color("green"),10))

        pixelColorSize = 5

        height = resultImage.shape[0]
        width = resultImage.shape[1]

        cursor_height = cursor.shape[0]
        cursor_width = cursor.shape[1]

        # Display vertivally
        if vertical:

            gradient_height = len(colors)*pixelColorSize
            start_position = height/2-gradient_height/2

            # Put Colors
            i = 0
            for color in colors:
                h = color.hex.lstrip('#')
                if(len(h) == 6):
                    cv2.rectangle(
                            resultImage,
                            (
                                int(width-avSusRectWidth),
                                int(start_position + i*pixelColorSize)
                            ),
                            (
                                int(width),
                                int(start_position + (i+1)*pixelColorSize)
                            ),
                            tuple(int(h[j:j+2], 16) for j in (4, 2, 0)),
                            -1)
                else:
                    cv2.rectangle(
                            resultImage,
                            (
                                int(width-avSusRectWidth),
                                int(start_position + i*pixelColorSize)
                            ),
                            (
                                int(width),
                                int(start_position + (i+1)*pixelColorSize)
                            ),
                            tuple(int(h[j:j+2], 16) for j in (2, 1, 0)),
                            -1)
                i += 1

            # Put cursor
            x_offset = width-avSusRectWidth-30
            y_offset = int((start_position - cursor_height/2) + gradient_height * (2-(value-1))/2)
            resultImage[y_offset:y_offset+cursor_height, x_offset:x_offset+cursor_width] = cursor

        else:

            # Display Horizontally
            gradient_height = len(colors)*pixelColorSize
            gradient_length = len(colors)*pixelColorSize
            start_position  = height/2-gradient_height/2

            # Put Colors
            i = 0
            for color in colors:
                h = color.hex.lstrip('#')
                if(len(h) == 6):
                    cv2.rectangle(
                        resultImage,
                         # Vertex
                         (
                            int((width- gradient_length)/2) + i*pixelColorSize,
                            int(0 + 4*FONT_SIZE*10)
                         ),
                         # Opposite Vertex
                         (
                            int((width - gradient_length)/2) + (i+1)*pixelColorSize,
                            int(pixelColorSize + 5 + 4*FONT_SIZE*10)
                         ),
                         tuple(int(h[j:j+2], 16) for j in (4, 2, 0)),
                         -1
                    )
                else:
                    cv2.rectangle(
                         resultImage,
                         # Vertex
                         (
                            int((width- gradient_length)/2) + i*pixelColorSize,
                            int(0 + 4*FONT_SIZE*10)
                         ),
                         # Opposite Vertex
                         (
                            int((width - gradient_length)/2) + (i+10)*pixelColorSize,
                            int(pixelColorSize + 5 + 4*FONT_SIZE*10)
                         ),
                         tuple(int(h[j:j+2], 16) for j in (2, 1, 0)),
                         -1
                    )
                i += 1

            # Put Cursor
            x_offset = int((width- gradient_length)/2 + gradient_length*(2-(value-1))/2 - cursor_height/2)
            y_offset = int(4*FONT_SIZE*10 + pixelColorSize  + cursor_height/2 -6)

            try:
                resultImage[y_offset:y_offset+cursor_height, x_offset: x_offset + cursor_width]=cursor
            except Exception as e:
                print("[+] Error putting cursor -> {}".format(e))


            # Put Text below Gradient
            text = "Evaluation Risque\n"

            self.put_multi_lines(
                    int((width-gradient_length)/2),
                    15,
                    text,
                    resultImage,
                    (0,0,0),
                    gradient_length+1, # Center text around gradient
                    True # Center to true
            )
            self.put_multi_lines(
                    int((width-gradient_length)/2),
                    15,
                    text,
                    resultImage,
                    NAME_COLOR,
                    gradient_length, # Center text around gradient
                    True # Center to true
            )

    def calculateAverageSuspicious(self, faceSuspicion):
        global averageSuspicious
        currentFaces = len(faceSuspicion) - faceSuspicion.count(0)
        if(currentFaces>0):
            averageSuspicious = sum(faceSuspicion)/currentFaces
            print(f"Average suspicion: {averageSuspicious}")


    def put_warning_message(self, fid, faceTrackers, faceSuspicion, resultImage):
        '''
        Print a Warning message depending on suspicion level
        '''
        tracked_position =  faceTrackers[fid].get_position()

        t_x = int(tracked_position.left())
        t_y = int(tracked_position.top())
        t_w = int(tracked_position.width())
        t_h = int(tracked_position.height())

        suspicion_level = faceSuspicion[fid]

        text = "Citoyen modele"
        color = GREEN
        if suspicion_level > SUSPICIOUS_THRESHOLD:
            text = "ATTENTION!\n INDIVIDU NUISIBLE!"
            color = RED
        elif suspicion_level > 1:
            text = "Citoyen Suspect. \n A Surveiller."
            color = ORANGE

        # Print to screen
        self.put_multi_lines(
                t_x,
                int(t_y) + 20 + t_h,
                text,
                resultImage,
                color,
                t_w, # Needed if want to center
                True # Center
                )

    def put_multi_lines(self, x, y, text, image, color = WHITE, w= 0, center = False):
        '''
        Print a multi line text on the screen
        Lines split based upon \n

        w -> width of text, only used if center == TRUE
        '''

        # If center, adjust text to picture
        if center == True:
            if w == 0:
                print("If you want to center multi line text, give me the width of the image")
                exit()

        # Split lites
        lines = text.split("\n")
        adj = 0


        # Print lines one below the other, adjusting for x if centering
        for i in lines:
            if center == True:
                x_align = x + int((w - len(i)*10)/2 + 10)
            else:
                x_align = x

            cv2.putText(image,
                        i,
                        (
                            # Name's alignment
                            x_align,
                            # Adjust Y for lines
                            y + int(adj*(FONT_SIZE +15))
                        ),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        FONT_SIZE,
                        color,
                        1,
                        cv2.LINE_AA)

            adj += 1

    def rectangle_around_face(self, fid, faceTrackers, faceSuspicion, draw_person_dangerosity, resultImage):
        '''
        Draw a rectangle around's people face
        If draw_person_dangerosity = True -> Add a color based on suspicionLevel
        '''
        tracked_position =  faceTrackers[fid].get_position()

        t_x = int(tracked_position.left())
        t_y = int(tracked_position.top())
        t_w = int(tracked_position.width())
        t_h = int(tracked_position.height())

        # If we want to have a dangerosity color per personn
        if draw_person_dangerosity :
            suspicionLevel = faceSuspicion[fid]
            person_color = suspiciousColors[suspicionLevel] #pick_person_dangerosity_color(suspicionLevel)

        # Else default to rectangleColor
        else:
            person_color = rectangleColor

        # Draw Rectangle
        cv2.rectangle(resultImage,
                      (t_x, t_y),
                      (t_x + t_w , t_y + t_h),
                      person_color,
                      2)

    def pick_person_dangerosity_color(self, suspicionLevel):
        '''
        Compute person level suspicion -> simplified to 2 levels
        Can reintroduce the faceSuspicious dictionnary lated, was more clean
        '''
        if suspicionLevel> SUSPICIOUS_THRESHOLD:
            color = rectangleColorSuspicious
        else:
            color = rectangleColor

        return color


    def __init__(self, src, videoName):
        self.videoName = videoName
        self.draw_person_dangerosity = True
        self.add_warning_message = True

        #variables holding the current frame number and the current faceid
        self.frameCounter = 0
        self.currentFaceID = 0

        #Variables holding the correlation trackers and the name per faceid
        self.faceTrackers = {}
        self.faceNames = {}
        self.faceSuspicion = list() # 0 is neutral/non existent person. 1 is ok, 2 is suspect, 3 is dangerous

        self.capture = cv2.VideoCapture(src)
        # Start the thread to read frames from the video stream
        self.thread_capture = Thread(target=self.update, args=())
        self.thread_capture.daemon = True
        self.thread_capture.start()


    def update(self):
        # Read the next frame from the stream in a different thread
        while True:
            if self.capture.isOpened():
                (self.status, self.frame) = self.capture.read()
                self.detectAndTrackMultipleFacesInFrame(self.frame)
            time.sleep(.01)

    def detectAndTrackMultipleFacesInFrame(self, resultImage):
        #Increase the framecounter
        self.frameCounter += 1

        #Update all the trackers and remove the ones for which the update
        #indicated the quality was not good enough
        fidsToDelete = []
        for fid in self.faceTrackers.keys():
            trackingQuality = self.faceTrackers[ fid ].update( resultImage )

            #If the tracking quality is not good enough, we must delete
            #this tracker
            if trackingQuality < 7:
                fidsToDelete.append( fid )

        for fid in fidsToDelete:
            print("Removing fid " + str(fid) + " from list of trackers")
            self.faceTrackers.pop( fid , None )
            self.faceSuspicion[fid] = 0

            self.calculateAverageSuspicious(self.faceSuspicion)

        #Every 10 frames, we will have to determine which faces
        #are present in the frame
        if (self.frameCounter % 10) == 0:

            #For the face detection, we need to make use of a gray
            #colored image so we will convert the baseImage to a
            #gray-based image
            gray = cv2.cvtColor(resultImage, cv2.COLOR_BGR2GRAY)
            #Now use the haar cascade detector to find all faces
            #in the image
            faces = faceCascade.detectMultiScale(gray, 1.3, 5)

            #Loop over all faces and check if the area for this
            #face is the largest so far
            #We need to convert it to int here because of the
            #requirement of the dlib tracker. If we omit the cast to
            #int here, you will get cast errors since the detector
            #returns numpy.int32 and the tracker requires an int
            for (_x,_y,_w,_h) in faces:
                x = int(_x)
                y = int(_y)
                w = int(_w)
                h = int(_h)


                #calculate the centerpoint
                x_bar = x + 0.5 * w
                y_bar = y + 0.5 * h

                #Variable holding information which faceid we
                #matched with
                matchedFid = None

                #Now loop over all the trackers and check if the
                #centerpoint of the face is within the box of a
                #tracker
                for fid in self.faceTrackers.keys():
                    tracked_position =  self.faceTrackers[fid].get_position()

                    t_x = int(tracked_position.left())
                    t_y = int(tracked_position.top())
                    t_w = int(tracked_position.width())
                    t_h = int(tracked_position.height())


                    #calculate the centerpoint
                    t_x_bar = t_x + 0.5 * t_w
                    t_y_bar = t_y + 0.5 * t_h

                    #check if the centerpoint of the face is within the
                    #rectangleof a tracker region. Also, the centerpoint
                    #of the tracker region must be within the region
                    #detected as a face. If both of these conditions hold
                    #we have a match
                    if ( ( t_x <= x_bar   <= (t_x + t_w)) and
                         ( t_y <= y_bar   <= (t_y + t_h)) and
                         ( x   <= t_x_bar <= (x   + w  )) and
                         ( y   <= t_y_bar <= (y   + h  ))):
                        matchedFid = fid

                        #We calculate distance between previous and new center of the tracker, to evaluate if the tracker move
                        #Not working with this method
                        #centerDistance = ((x_bar - t_x_bar) ** 2 + (y_bar - t_y_bar) ** 2) ** 0.5
                        #print("Distance : " + str(centerDistance))
                        #if(centerDistance>8):
                            #print("Target " + self.faceNames[ matchedFid ] + " is moving")

                #If no matched fid, then we have to create a new tracker
                if matchedFid is None:

                    suspicionLevel = random.randint(1,3)
                    self.faceSuspicion.append(suspicionLevel)
                    print("Creating new tracker " + str(self.currentFaceID) + " with suspicion level: " + str(suspicionLevel))
                    #Create and store the tracker
                    tracker = dlib.correlation_tracker()
                    tracker.start_track(resultImage,
                                        dlib.rectangle( x-10,
                                                        y-20,
                                                        x+w+10,
                                                        y+h+20))

                    self.faceTrackers[ self.currentFaceID ] = tracker

                    # Pick up random name: Do we want a thread for that?
                    # Advantage is that we can pop an "Identifying..." Message
                    # Disadvantage is that is more "lourd"

                    # Directly add name + pass a charset to which name must belong
                    # "Person " + str(fid)
                    randomName = randomnames.rand_name_statistic("letters")
                    print("New target detected: " + randomName)
                    self.faceNames[ self.currentFaceID ] = randomName

                    self.calculateAverageSuspicious(self.faceSuspicion)

                    #Increase the currentFaceID counter
                    self.currentFaceID += 1

        #Now loop over all the trackers we have and draw the rectangle
        #around the detected faces. If we 'know' the name for this person
        #(i.e. the recognition thread is finished), we print the name
        #of the person, otherwise the message indicating we are detecting
        #the name of the person
        for fid in self.faceTrackers.keys():

            # Draw a rectangle around people's face
            self.rectangle_around_face(fid,
                                  self.faceTrackers,
                                  self.faceSuspicion,
                                  # If False, colors will not change with suspicion level
                                  self.draw_person_dangerosity,
                                  resultImage)

            # Deal with the two others rectangles
            tracked_position = self.faceTrackers[fid].get_position()

            t_x = int(tracked_position.left())
            t_y = int(tracked_position.top())
            t_w = int(tracked_position.width())
            t_h = int(tracked_position.height())

            self.displayGradientOn(resultImage, averageSuspicious) # TODO optimization ?

            # If Known -> print Name
            if fid in self.faceNames.keys():

                cv2.putText(resultImage,
                    self.faceNames[fid] ,
                    # Name's alignment
                    (t_x+ int((t_w - len(self.faceNames[fid])*10)/2 + 10), int(t_y) - 10),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    FONT_SIZE,
                    (0,0,0),
                    2,
                    cv2.LINE_AA)

                cv2.putText(resultImage,
                    self.faceNames[fid] ,
                    # Name's alignment
                    (t_x+ int((t_w - len(self.faceNames[fid])*10)/2 + 10), int(t_y) - 10),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    FONT_SIZE,
                    NAME_COLOR,
                    1,
                    cv2.LINE_AA)

                # Add a warning message depending on suspicious level
                if self.add_warning_message:
                    self.put_warning_message(fid,
                                        self.faceTrackers,
                                        self.faceSuspicion,
                                        resultImage)

            # Else print Identifying
            else:
                text = "Identifying"
                cv2.putText(resultImage,
                            text ,
                            (t_x + int((t_w - len(text)*10)/2 + 10), int(t_y)),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            0.5,
                            IDENTIFYING_COLOR,
                            1,
                            cv2.LINE_AA)

    def show_frame(self):
        # Display frames in main program
        cv2.imshow(self.videoName, self.frame)
        key = cv2.waitKey(1)
        if key == ord('q'):
            self.capture.release()
            cv2.destroyAllWindows()
            exit(1)


if __name__ == '__main__':
    camera_list = list_available_cameras()

    facetrackerList = []
    for camera_id in camera_list:
        videoName = "cam" + str(camera_id)

        facetrackerList.append(facetracker(camera_id, videoName))

    data={}
    try:
        with open('windows_pos.json') as f:
            data = json.load(f)
    except EnvironmentError: # parent of IOError, OSError *and* WindowsError where available
        print ('Windows position file not found: random position')
    else:
        print(data)
# To move windows to the previous positions
        i = 0
        for camera in facetrackerList:
            cv2.namedWindow(camera.videoName, cv2.WINDOW_NORMAL)
            cv2.resizeWindow(camera.videoName, int(data["WindowsList"][i]["width"]), int(data["WindowsList"][i]["height"]))
            cv2.moveWindow(camera.videoName, int(data["WindowsList"][i]["X"]), int(data["WindowsList"][i]["Y"]))
            i+=1

    while True:
        try:
            for camera in facetrackerList:
                camera.show_frame()
        except AttributeError:
            pass
