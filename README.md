# FacialRecoTrackingScore

From https://github.com/gdiepen/face-recognition / track multiple faces

Project to be shown in Marseille on the 30th of january, 2020, for the Technopolice project.

## Installation

Need opencv, opencv-contrib, dlib, colour

```
pip install opencv-python
pip install opencv-contrib-python
pip install dlib
pip install colour
```

### On Archlinux and derivatives :

    yay -S python-opencv python-colour python-dlib


## Usage

    ./trackmultiplefaces.py

## Autostart

For autostart with gnome desktop, update the *.desktop file to reflect your environment, then copy it into your ~/.config/autostart/